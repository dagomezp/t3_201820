package model.data_structures;

import java.util.Iterator;

public class Queue<E> implements IQueue {

	/**
	 * Atributo que indica la cantidad de elementos que han sido almacenados en la lista.
	 */
	protected int cantidadElementos;

	/**
	 * Primer nodo de la lista.
	 */
	protected Node<E> primerNodo;

	/**
	 *  ultimo nodo de la lista.
	 */
	protected Node<E> ultimo;

	
	
	public Queue(){
		primerNodo = null;
		cantidadElementos = 0;
		ultimo = null;
	}

	public Queue(E primero){
		primerNodo = new Node<E>(primero);
		cantidadElementos = 1;
		ultimo = primerNodo;
	}






	public Node<E> getPrimerNodo(){
		return primerNodo;
	}
	
	public Node<E> ultimoNodo(){
		return ultimo;
	}
	
	
	@Override
	public Iterator iterator() {
		
		IteradorLista<E> iterador = new IteradorLista<E>(primerNodo);
		return iterador;
	}

	@Override
	public boolean isEmpty() {

		Boolean isEmpty = true;
		if(primerNodo!=null){
			isEmpty = false;
		}
		return isEmpty;
	}

	@Override
	public int size() {

		return cantidadElementos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void enqueue(Object t) {
		
		Node<E> nuevo = new Node<E>((E) t);
		if(primerNodo!=null){
			ultimo.cambiarSiguiente(nuevo);
			nuevo.cambiarAnterior(ultimo);
			ultimo = nuevo;
			cantidadElementos++;
			
		}
		else{
			primerNodo = nuevo;
			ultimo = nuevo;
			cantidadElementos++;
		}
		
		

	}

	@Override
	public Object dequeue() {
		// TODO Auto-generated method stub
		Object sacado = null;
		
		if(primerNodo!=null){
			sacado = primerNodo;
			Node<E> siguiente = primerNodo.darSiguiente();
			if(siguiente!=null){
				primerNodo = siguiente;
				siguiente.cambiarAnterior(null);
				cantidadElementos--;
			}
			else{
				primerNodo = null;
				cantidadElementos--;
			}
		}
		
		return sacado;
	}

}
