package model.logic;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Iterator;

import com.opencsv.CSVReader;

import api.IDivvyTripsManager;
import model.data_structures.DobleLinkedList;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.Node;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.VOBike;
import model.vo.VOTrip;

public class DivvyTripsManager implements IDivvyTripsManager {

	/**
	 * Cola que contiene los viajes realizados por una bicileta en particular
	 */
	private Queue<VOTrip> qBikeTrips;
	/**
	 * Pila que contiene los viajes realizados por una bicicleta en particular
	 */
	private Stack<VOTrip> sBikeTrips;


	private DobleLinkedList<VOBike> bikeList;
	/**
	 * Lista doble encadenada de la totalidad de viajes realizados
	 */
	private DobleLinkedList<VOTrip> tripList;
	/**
	 * Pila con la totalidad de los viajes realizados
	 */
	private Stack<VOTrip> tripStack ;
	/*
	 * Cola con la totalidad de los viajes realizados
	 */
	private Queue<VOTrip> tripQueue;

	public DivvyTripsManager()
	{
		qBikeTrips = new Queue<>();
		sBikeTrips = new Stack<>();
		bikeList = new DobleLinkedList<>();
		tripList= new DobleLinkedList<>();
		tripStack= new Stack<>();
		tripQueue= new Queue<>();

	}

	public void loadStations (String stationsFile) {

		try{

			CSVReader reader = new CSVReader(new FileReader(stationsFile));
			String [] dato;
			reader.readNext();

			while ((dato =reader.readNext()) != null) {
				VOBike nuevo=new VOBike(dato[0], dato[1], dato[2],dato[3],dato[4],dato[5], dato[6]);
				bikeList.addAtFirst(nuevo);//La complejidad de agregar al final en una lista simple es mucho mayor en esta implementación
				
			}
			System.out.println(bikeList.getSize());
			reader.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}
	// TODO Auto-generated method stub




	public void loadTrips (String tripsFile) {

		try{

			CSVReader reader = new CSVReader(new FileReader(tripsFile));
			String [] dato;
			reader.readNext();
			int cont=0;

			while ((dato =reader.readNext()) != null) {
				VOTrip nuevo = new VOTrip(dato[0],dato[1],dato[2],dato[3],dato[4],dato[5],dato[6],dato[7],dato[8],dato[9],dato[10],dato[11]);
				tripList.addAtFirst(nuevo);
				cont ++;
				System.out.println(cont);
				
			}
			System.out.println(tripList.getSize());
			reader.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}



	/**
	 * Guarda los viajes hechos por una bicicleta específica en una cola y una pila.
	 * @param bikeId. Id de la bicicleta.
	 */
	public void loadBikeTrips(String bikeId)
	{

		Iterator<VOTrip>iter = tripList.iterator();
		while(iter.hasNext())
		{
			if(iter.next().getBikeId().equals(bikeId)){
				qBikeTrips.enqueue(iter.next());
				sBikeTrips.push(iter.next());
			}
		}


	}

	@Override
	public IDoublyLinkedList <String> getLastNStations (int bicycleId, int n) {
		// TODO Auto-generated method stub



		System.out.println("Entr� al m�todo");
		DobleLinkedList<String> estaciones = new DobleLinkedList<String>();

		int contador = 1;
		Node<VOTrip> actual = tripList.darPrimerNodo();
		int contador2 = 0;

		while( actual.darSiguiente()!=null && contador <= n){
			System.out.println(contador);
			if(actual.darElemento().getBikeId().equals(Integer.toString(bicycleId))){
				estaciones.addAtFirst(actual.darElemento().getFromStation());
				contador++;
			}
			actual = actual.darSiguiente();
			System.out.println(contador2);
			contador2++;

		}

		return estaciones;
	}

	@Override
	public VOTrip customerNumberN (int stationID, int n) {

		System.out.println("Entr� al m�todo");
		Node<VOTrip> actual = tripList.darPrimerNodo();
		VOTrip viajeN = null;

		int contador = 0;

		while(actual.darSiguiente()!=null && contador < n){
			System.out.println(contador);
			if(actual.darElemento().gettStationId().equalsIgnoreCase(Integer.toString(stationID))){
				viajeN = actual.darElemento();
				contador++;
				actual = actual.darSiguiente();

			}
			else{
				actual = actual.darSiguiente();
			}

		}



		return viajeN;
	}	


}
