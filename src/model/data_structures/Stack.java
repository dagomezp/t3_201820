package model.data_structures;

import java.util.Iterator;

public class Stack<E> implements IStack<E> {


	/**
	 * Atributo que indica la cantidad de elementos que han sido almacenados en la lista.
	 */
	protected int cantidadElementos;

	/**
	 * Primer nodo de la lista.
	 */
	protected Node<E> primerNodo;

	/**
	 *  nodo actual de la lista.
	 */
	protected Node<E> actual;

	/**
	 * Constructor vacío
	 */
	public Stack(){
		
		primerNodo = null;
		cantidadElementos = 0;
		actual = null;
	}

	/**
	 * Constructor con primer Nodo
	 * @param primero primer nodo del Stack
	 */
	public Stack(E primero){
		
		primerNodo = new Node<E>(primero);
		cantidadElementos = 1;
		actual = primerNodo;
	}



	@Override
	public Node<E> darPrimerNodo(){
		return primerNodo;
	}

	@Override
	public Iterator<E> iterator() {
		
		IteradorLista<E> iterador = new IteradorLista<E>(primerNodo);
		return iterador;
	}

	@Override
	public boolean isEmpty() {

		Boolean isEmpty = true;
		if(primerNodo!=null){
			isEmpty = false;
		}
		return isEmpty;
	}

	@Override
	public int size() {
		
		return cantidadElementos;
	}

	@Override
	public void push(E t) {
		
		Node<E> nuevo = new Node<E>(t);
		if(primerNodo!=null){
			nuevo.cambiarSiguiente(primerNodo);
			primerNodo.cambiarAnterior(nuevo);
			primerNodo = nuevo;
			cantidadElementos++;
		}
		else{
			primerNodo = nuevo;
			cantidadElementos++;
		}
	}

	@Override
	public E pop() {

		E elemento = null;
		if(primerNodo!=null){
			if(primerNodo.darSiguiente()!=null){
				elemento = primerNodo.darElemento();
				primerNodo = primerNodo.darSiguiente();
				primerNodo.cambiarAnterior(null);
				cantidadElementos--;
			}
			else{
				elemento = primerNodo.elemento;
				primerNodo = null;
				cantidadElementos--;
			}
		}
		return elemento;
	}

}
