package model.data_structures;

import java.util.Iterator;

public class DobleLinkedList<E> implements IDoublyLinkedList<E> {

	/**
	 * Atributo que indica la cantidad de elementos que han sido almacenados en la lista.
	 */
	protected int cantidadElementos;

	/**
	 * Primer nodo de la lista.
	 */
	protected Node<E> primerNodo;

	/**
	 *  nodo actual de la lista.
	 */
	protected Node<E> actual;



	public void DobleLinkedList(){
		primerNodo = null;
		cantidadElementos = 0;
		actual = null;
	}

	public void DobleLinkedList(E primero){
		primerNodo = new Node<E>(primero);
		cantidadElementos = 1;
		actual = primerNodo;
	}

	@Override
	public Iterator<E> iterator() {
		// TODO Auto-generated method stub
		IteradorLista<E> iterador = new IteradorLista<E>(primerNodo);
		return iterador;
	}


	public Integer getSize() {
		// TODO Auto-generated method stub
		return cantidadElementos;
	}

	public void addAtFirst(E elemento) {
		// TODO Auto-generated method stub
		Node<E> nuevo = new Node<E>(elemento);
		if(primerNodo!=null){
			nuevo.cambiarSiguiente(primerNodo);
			primerNodo.cambiarAnterior(nuevo);
			primerNodo = nuevo;
			cantidadElementos++;
		}
		else{
			primerNodo = nuevo;
			primerNodo.cambiarSiguiente(null);
			primerNodo.cambiarAnterior(null);
		}

	}
	
	public Node<E> darPrimerNodo(){
		return primerNodo;
	}


	public void addAtEnd(E elemento) throws Exception {
		// TODO Auto-generated method stub

		Node<E> ultimo = new Node<E>(elemento);
		if(primerNodo==null){
			primerNodo = ultimo;
		}
		else{
			if(primerNodo.darSiguiente()==null){

				if(primerNodo.darElemento().equals(elemento)){
					throw new Exception("El elemento a agregar ya existe");
				}

				primerNodo.cambiarSiguiente(ultimo);
				ultimo.cambiarAnterior(primerNodo);
				cantidadElementos++;
			}
			else{
				Node<E> actual = primerNodo;

				while(actual.darSiguiente()!=null){

					if(actual.darElemento().equals(elemento)){
						throw new Exception("El elemento a agregar ya existe");
					}

					actual = actual.darSiguiente();
				}

				actual.cambiarSiguiente(ultimo);
				ultimo.cambiarAnterior(actual);
				cantidadElementos++;
			}
		}
	}



	public E getElement(int index) {
		// TODO Auto-generated method stub

		if(index < 0 || index >= cantidadElementos){
			throw new IndexOutOfBoundsException("No existe ningún elemento en esta posición");
		}

		else{
			E elemento = null;
			Node<E> actual = primerNodo;
			int indice = 0;
			while (indice < index){
				actual = actual.darSiguiente();
				elemento = actual.darElemento();
				indice++;
			}
			return elemento;
		}
	}


	public E getCurrentELement() {
		// TODO Auto-generated method stub
		return actual.darElemento();
	}


	public void delete(E elemento) throws Exception {
		// TODO Auto-generated method stub

		if(primerNodo.darElemento().equals(elemento)){
			if(primerNodo.darSiguiente()!=null){
				primerNodo = primerNodo.darSiguiente();
				primerNodo.cambiarAnterior(null);
			}
			else{
				primerNodo = null;
			}
		}
		else{
			Node<E> actual = primerNodo;
			Boolean encontrado = false;
			while(actual.darSiguiente()!=null && !encontrado){
				if(actual.darSiguiente().darElemento().equals(elemento)){
					encontrado = true;
				}
				else{
					actual = actual.darSiguiente();
				}
			}
			if(encontrado){
				actual.cambiarSiguiente(actual.darSiguiente().darSiguiente());
			}
			else{
				throw new Exception("No existe el elemento que se quiere eliminar");
			}


		}
	}




	public void deleteAtK(int index) {
		// TODO Auto-generated method stub

		// TODO Completar según la documentación
		E elemento = null;
		if(index < 0 || index >= cantidadElementos){
			throw new IndexOutOfBoundsException("No existe ningún elemento en esta posición");
		}
		else{
			Node<E> actual = (Node<E>) primerNodo;
			Node<E> siguiente = (Node<E>) actual.darSiguiente();
			if(index == 0){
				elemento = primerNodo.darElemento();
				primerNodo = (Node<E>) actual.darSiguiente();
				cantidadElementos--;
				if(primerNodo != null){
					((Node<E>) primerNodo).cambiarAnterior(null);
				}
			}
			else{
				int indice=0;

				while(siguiente != null && index-1!= indice && index>1){
					actual = (Node<E>) actual.darSiguiente();
					siguiente = (Node<E>) actual.darSiguiente();
					indice++;
				}
				elemento = siguiente.darElemento();
				if(siguiente.darSiguiente()!=null){
					actual.cambiarSiguiente(siguiente.darSiguiente());
					((Node<E>) actual.darSiguiente()).cambiarAnterior(actual);
					cantidadElementos--;
				}
				else{
					actual.cambiarSiguiente(null);
					cantidadElementos--;
				}
			}
		}

	}


	public E next() {
		// TODO Auto-generated method stub
		actual = actual.darSiguiente();
		return actual.darElemento();
	}


	public E previous() {
		// TODO Auto-generated method stub
		actual = actual.darAnterior();
		return actual.darElemento();
	}




}
