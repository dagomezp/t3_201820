package model.data_structures;


public class Node<E> {
	
	/**
	 * Elemento almacenado en el nodo.
	 */
	protected E elemento;
	
	/**
	 * Siguiente nodo.
	 */
	//TODO Defina el atributo siguiente como el siguiente nodo de la lista.
	private Node<E> siguiente;
	
	/**
	 * Nodo anterior.
	 */
	//TODO Defina el atributo anterior como el nodo anterior de la lista.
	Node<E> anterior;
	
	/**
	 * Constructor del nodo.
	 * @param elemento El elemento que se almacenará en el nodo. elemento != null
	 */
	public Node(E elemento)
	{
		//TODO Completar de acuerdo a la documentación.
		this.elemento = elemento;
		siguiente = null;
	}
	
	/**
	 * Método que cambia el siguiente nodo.
	 * <b>post: </b> Se ha cambiado el siguiente nodo
	 * @param siguiente El nuevo siguiente nodo
	 */
	public void cambiarSiguiente(Node<E> siguiente)
	{
		//TODO Completar de acuerdo a la documentación.
		this.siguiente = siguiente;
	}
	
	/**
	 * Método que retorna el elemento almacenado en el nodo.
	 * @return El elemento almacenado en el nodo.
	 */
	public E darElemento()
	{
		//TODO Completar de acuerdo a la documentación.
		return elemento;
	}
	
	/**
	 * Cambia el elemento almacenado en el nodo.
	 * @param elemento El nuevo elemento que se almacenará en el nodo.
	 */
	public void cambiarElemento(E elemento)
	{
		//TODO Completar de acuerdo a la documentación.
		this.elemento = elemento;
	}
	
	/**
	 * Método que retorna el identificador del nodo.
	 * Este identificador es el identificador del elemento que almacena.
	 * @return Identificador del nodo (identificador del elemento almacenado).
	 */

	
	/**
	 * Método que retorna el siguiente nodo.
	 * @return Siguiente nodo
	 */
	public Node<E> darSiguiente()
	{
		//TODO Completar de acuerdo a la documentación.
		return siguiente;
	}
	
	/**
	 * Método que retorna el nodo anterior.
	 * @return Nodo anterior.
	 */
	public Node<E> darAnterior()
	{
		//TODO Completar de acuerdo a la documentación.
		return anterior;
	}
	
	public void cambiarAnterior(Node<E> anterior)
	{
		//TODO Completar de acuerdo a la documentación.
		this.anterior = anterior;
	}

}
