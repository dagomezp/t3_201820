package model.vo;

/**
 * Representation of a byke object
 */
public class VOBike {

	private String id;
	private String longitude;
	private String latitude;
	private String dpCapacity;
	private String onlineDate;
	private String name;
	private String city;
	
	

	public VOBike (String pId, String pName, String pCity, String pLatitude, String pLongitude, String pCapacity, String pDate)
	{
		id=pId;
		name=pName;
		city=pCity;
		longitude=pLongitude;
		latitude=pLatitude;
		dpCapacity=pCapacity;
		onlineDate=pDate;
	}
	
	
	/**
	 * @return id_bike - Bike_id
	 */
	public String getid() {
		// TODO Auto-generated method stub
		return id;
	}	
	
	public String getLongitude()
	{
		return longitude;
	}
	
	public String getLatitude()
	{
		return latitude;
	}
	public String getCapacity() {
		// TODO Auto-generated method stub
		return dpCapacity;
	}	
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}	
	public String getDate() {
		// TODO Auto-generated method stub
		return onlineDate
				;
	}	
}
